# ASR 2018 Project 1
## To Generate features.
#### Run 
```
python import_timit.py --timit=path_to_folder_to_train --n_delta=n_delta_value
```

## To read features from folder features and train for the following models
- MFCC
	- With energy coefficient
		- Number of mixtures is 64
	- Without energy coefficient i.e 0th coeffient is 0
		- Number of mixtures is 2,4,8,16,32,64,128
- MFCC Delta
	- With energy coefficient
		- Number of mixtures is 64
	- Without energy coefficient i.e 14th coeffient is 0
		- Number of mixtures is 64
- MFCC Delta Delta
	- With energy coefficient
		- Number of mixtures is 64
	- Without energy coefficient i.e 27th coeffient is 0
		- Number of mixtures is 64
- Folder structure
	```
	.
	+-- features
	|	+-- mfcc
	|	|	+-- timit.hdf
	|	+-- mfcc_delta
	|	|	+-- timit.hdf
	|	+-- mfcc_delta_delta
	|	|	+-- timit.hdf
	+-- train.py

	```

#### Run
``` 
python train.py
```

## To test on a features dump specified and the best model and return predicted list of phonemes
####Run
```
python test.py --test_path="location_to_test_path"
```

## To test on all features dumps generated and all models and return accuracy for each
- Folder structure
	- feature dump has the following stucture
		```
		.
		+-- test
		|	+-- mfcc
		|	|	+-- test.hdf
		|	+-- mfcc_delta
		|	|	+-- test.hdf
		|	+-- mfcc_delta_delta
		|	|	+-- test.hdf
		+-- test.py

		```
	- Models
		```
		.
		+-- models
		|	+-- with
		|	|	+-- mfcc
		|	|	|	+--002
		|	|	|	|	+--  *.pkl
		|	|	+-- mfcc_delta
		|	|	|	+--002
		|	|	|	|	+--  *.pkl
		|	|	+-- mfcc_delta_delta
		|	|	|	+--002
		|	|	|	|	+--  *.pkl
		|	+-- without
		|	|	+-- mfcc
		|	|	|	+--002
		|	|	|	|	+--  *.pkl
		|	|	+-- mfcc_delta
		|	|	|	+--002
		|	|	|	|	+--  *.pkl
		|	|	+-- mfcc_delta_delta
		|	|	|	+--002
		|	|	|	|	+--  *.pkl
		+-- test.py

		```
#### Run
```
python test_all.py
```

## To test generate features given a single utterance

#### Run
```
python predict_file.py --wav=path_to_wavfile --phn=path_to_phn_file
```
